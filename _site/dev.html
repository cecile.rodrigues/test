<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>R - Avancé</title>
    <meta charset="utf-8" />
    <meta name="author" content="Cécile Rodrigues - CNRS / CERAPS" />
    <script src="libs/header-attrs-2.17/header-attrs.js"></script>
    <link href="libs/remark-css-0.0.1/default.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/default-fonts.css" rel="stylesheet" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

.title[
# R - Avancé
]
.subtitle[
## Université de Lille - M2 Sociologie - Parcours ENSP
]
.author[
### Cécile Rodrigues - CNRS / CERAPS
]
.date[
### Dernière modification : 23/11/2022
]

---


## Créer des fonctions dans R

Dans R, on crée des objets et on leur applique des fonctions pour produire des résultats et des analyses. Les fonctions prennent (1) un ou plusieurs **arguments en entrée** (ou *paramètres* ou *input*), éventuellement certaines **options**, (2) effectuent des **opérations ou des actions** et (3) produisent et renvoient des **résultats ou sorties** (*output*).

R est quand même un langage de programmation qui permet de créer ses propres fonctions si on ne les trouve pas dans le langage de base ou via un package.

L'écriture, dans sa version la plus basique, d'une fonction sera :


```r
Ajoute2 &lt;- function(x){
  res &lt;- x + 2
  return(res)
}
Ajoute2(4)
```

- x est l'argument, qui est donné en entrée par l'utilisateur·rice de la fonction
- res &lt;- x+2 est l'action : "Ajouter 2 au paramètre x"
- return(res) renvoie le résultat, soit dans la console s'il n'y a rien à gauche, soit dans un objet si la fonction est précédée de `objet &lt;-`.

---
## Créer des fonctions dans R


`Ajoute2` est apparu dans la partie "Functions" de l'environnement.

**Attention** : Si le nom de la fonction existait déjà, l'ancienne fonction est écrasée


```r
summary &lt;- function(x){
  res &lt;- x+2
  return(res)}
```

`summary` est maintenant remplacée par le contenu de `Ajoute2`.

Mais on peut toujours supprimer la fonction `summary` qu'on vient de créer localement :


```r
rm(summary)
```

---

## Exemple d'une fonction qui renvoie le pourcentage d'un vecteur


```r
propTable &lt;- function(v){
  tri &lt;- table(v)
  total &lt;- length(v)
  tri &lt;- round(tri/total*100,2)
  return(tri)
}
propTable(coco$q01)
```

---

## Exemple d'une fonction qui renvoie un graphique


Ca peut être une fonction qui intègre la création d'un graphique :


```r
MyBarplot &lt;- function(var){
  tri &lt;- table(var)
 barplot(tri, col ="skyblue", border=NA)
}
MyBarplot(coco$q01)
```

---
## Pourquoi utiliser des fonctions ?

--

Les fonctions sont très utiles quand on se retrouve à **répéter des actions**. Par exemple, on recode une dizaine de variables sur le même modèle : 1 = Oui et 2 = Non :

Au lieu d'écrire...


```r
library(questionr)
library(tidyverse)
```

```
## ── Attaching packages ─────────────────────────────────────── tidyverse 1.3.2 ──
## ✔ ggplot2 3.3.6      ✔ purrr   0.3.5 
## ✔ tibble  3.1.8      ✔ dplyr   1.0.10
## ✔ tidyr   1.2.1      ✔ stringr 1.4.1 
## ✔ readr   2.1.3      ✔ forcats 0.5.2 
## ── Conflicts ────────────────────────────────────────── tidyverse_conflicts() ──
## ✖ dplyr::filter() masks stats::filter()
## ✖ dplyr::lag()    masks stats::lag()
```

```r
# freq(coco$q24_1)
# coco$q24_1_r &lt;- fct_collapse(factor(coco$q24_1),
#                              "Oui"="1", "Non"="2", "NR"="6666")
# freq(coco$q24_1_r)
```

---
## Pourquoi utiliser des fonctions ?

... On crée la fonction qui fait ce recodage et on l'applique :


```r
fct_ON &lt;- function(x){
  recod &lt;- fct_collapse(factor(x),
  "Oui"="1", "Non"="2", "NR"="6666")
  return(recod)
}
coco$q24_2_r &lt;- fct_ON(coco$q24_2)
freq(coco$q24_2_r)
```

**Les intérêts sont :**
- On évite une série de copier/coller qui alourdissent le code ;
- Plus simple pour corriger d'éventuelles erreurs : on corrige la fonction et pas les 7 copies
- Réutilisable d'un script à l'autre, vous pouvez faire des scripts avec vos fonctions habituelles

Comme pour les fonctions en général, les arguments peuvent être explicités ou être implicites selon leur position : `fct_ON(x = coco$q24_2)` ou `fct_ON(coco$s24_2)` car il n'y a pour l'instant qu'un seul argument.

En général, on compte sur l'implicite pour les données et on explicite les options; Comme dans 
`mean(x, na.rm = T)`

---
## Pourquoi utiliser des fonctions ?

Un autre intérêt sera de tester un morceau de code en changeant un ou quelques paramètres :

```r
graphede &lt;- function(x){
des &lt;- data.frame(jets=sample(1:6, x, rep=T))
ggplot(des)+aes(x=jets)+geom_bar(fill="white",col="black")+theme_bw()+scale_x_continuous(breaks = 1:6)
}
graphede(500)
```

![](dev_files/figure-html/unnamed-chunk-8-1.png)&lt;!-- --&gt;


---
## Les valeurs par défaut

Il est possible d'intégrer des valeurs par défaut comme paramètres :


```r
Nom &lt;- function(x, dec=1, useNA="ifany"){}
```

--

Sans valeur par défaut, un argument est obligatoire mais facultatif si une valeur par défaut apparaît.

En **ajoutant ...**, on indique qu'on intègre les arguments par défaut des fonctions appelées dans celle qu'on crée :


```r
Nom &lt;- function(x, ...){}
```


---
## Objets locaux et objets globaux

- Objet dans l'environnement = **global** / Objet dans la fonction = **local**
- Une fonction peut accéder à un objet extérieur -&gt; existant dans l'environnement
- Mais les arguments et objets **créés dans la fonction** sont **prioritaires**
- Un objet créé dans une fonction n'existe **que** dans la fonction
- Les objets locaux sont détruits quand on sort de la fonction

---

## Return

Par défaut, R renvoie le dernier objet présent dans la fonction. Mais en utilisant la fonction `return`, on peut davantage maîtriser ce qui est renvoyé. C'est par exemple utile quand on veut renvoyer un élément différent sur la base de conditions.

**Comportement pas défaut :**


```r
fun &lt;- function(x){
  x+2
  x+5
}
fun(2)
```

--

Pour forcer le retour de la 1ère ligne :

```r
fun &lt;- function(x){
  return(x+2)
  x+5}
fun(2)
```


---
## Exercice

Créez une fonction qui renvoie le carré et le cube d'un nombre : ^ pour élever un chiffre à une puissance données : `2^2`.

--


```r
carrecube &lt;- function(x) {
  carre = x^2
  cube = x^3
  return(list(x = x,
              carre = carre,
              cube=cube))
}
```

On renvoie une liste qui contient le nombre mis en entrée, son carré et son cube. Si on fait `carrecube("Coucou")` ?

--

On obtient une erreur qu'on peut éviter avec `return` car une fois qu'une fonction `return` est actionnée, le reste de la fonction n'est pas exécuté.

---
## Exercice

Créez une fonction qui renvoie le carré et le cube d'un nombre : ^ pour élever un chiffre à une puissance données : `2^2`.



```r
carrecube &lt;- function(x) {
  if(!is.numeric(x)) return("Eh non. Il faut mettre un chiffre !")
     carre = x^2
     cube = x^3
     return(list(x = x,
                 carre = carre,
                 cube=cube))}
carrecube("coucou")
```

```
## [1] "Eh non. Il faut mettre un chiffre !"
```

*Note : Si on met "print" au lieu de "return", le code continue à s'exécuter et on rencontre une erreur.*

---
## Global / local

On n'est pas obligé·es de définir les arguments dans la fonction (local), ils peuvent être dans l'environnement global (la session rstudio). Mais si le même objet est dans la fonction, il sera priorisé.

Il est aussi possible de modifier un objet dans l'environnement global à partir d'une fonction en utilissant l'opérateur `&lt;&lt;-` :

```r
x &lt;- 4
changex &lt;- function(a){x &lt;&lt;- a}
x
```

```
## [1] 4
```

```r
changex(5)
x
```

```
## [1] 5
```

---

## Exercice

### Créez une fonction qui nous indique si on nombre est premier ou non

*Un nombre est premier s'il n'est divisible que par lui-même et par 1, c'est-à-dire si le reste de la division est égal à 0 uniquement quand on le divise par lui-même ou 1.*

- Dans R, on connaît le reste d'une division à l'aide de l'opérateur %% : `4%%2`
- Une propriété des nombres premiers : Si un nombre n'est divisible que par 1 et aucun autre nombre premier inférieur ou égal à sa racine carré, il sera premier.

1. Il faut envisager les arguments en entrée
2. Il faut envisager les entrées possibles et donc les erreurs possibles -&gt; Différents scénarios suivant ce que les utilisateur·rice mettent en entrée
3. Ecrire les warnings en conséquence
4. Envisager les cas de figure sans erreur et les sorties en conséquence

### Créez une fonction qui renvoie tous les nombres premiers entre deux valeurs

---

## Exercice

### Créez une fonction qui nous indique si on nombre est premier ou non


```r
premier &lt;- function(x){
  # Pas tout seul-----
  if(length(x)&gt;1) return(print("x doit être un nombre seul"))
  # Pas un nombre --------
  if(! class(x) %in% c("integer", "numeric")) return(print("x doit être un nombre"))
  # Pas entier ---------
  if(grepl("\\.",x)) return(print("x doit être un nombre entier"))
  # le nombre est-il premier ? -------
    for(i in (2:(x^.5))){
      if(x %% i == 0) return(paste0(x, " n'est pas premier"))}
    return(paste0(x, " est premier"))
}
premier(53)
```

```
## [1] "53 est premier"
```

---

## Exercice

### Créez une fonction qui renvoie tous les nombres premiers entre deux valeurs


```r
listepremier &lt;- function(x,y){
  liste &lt;- NULL
for (i in (x:y)){
 for(j in (2:(i^.5))){
   if(i %% j == 0){liste &lt;- c(liste, i)}
 }}
premiers &lt;- setdiff(x:y, liste)
return(paste0(paste0("Les nombres premiers entre ",x," et ",y, " sont : "),
         paste(premiers, collapse = ", ")))  
}
listepremier(10,20)
```

```
## [1] "Les nombres premiers entre 10 et 20 sont : 11, 13, 17, 19"
```


---

## Ressources

- Julien Barnier, *Introduction à R et au tidyverse*, Partie 14, Écrire ses propres fonctions,  https://juba.github.io/tidyverse/14-fonctions.html
- R Coders, Creating functions, https://r-coder.com/function-r/



    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"ratio": "16:9",
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
// add `data-at-shortcutkeys` attribute to <body> to resolve conflicts with JAWS
// screen reader (see PR #262)
(function(d) {
  let res = {};
  d.querySelectorAll('.remark-help-content table tr').forEach(tr => {
    const t = tr.querySelector('td:nth-child(2)').innerText;
    tr.querySelectorAll('td:first-child .key').forEach(key => {
      const k = key.innerText;
      if (/^[a-z]$/.test(k)) res[k] = t;  // must be a single letter (key)
    });
  });
  d.body.setAttribute('data-at-shortcutkeys', JSON.stringify(res));
})(document);
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
